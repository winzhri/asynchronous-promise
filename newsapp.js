// Get API GET https://newsapi.org/v2/top-headlines?country=us&apiKey=cef4629403514db3a22b403bd2e1631b

const apiKey = "e9f0453a69b24e3fb4113eb214dd5662";
const searchBar = document.querySelector("#search-bar");
const newsDirectory = document.querySelector("#news-directory");
const load = document.querySelector("#load");

function updateNewsList(searchTerm) {
  const url = `https://newsapi.org/v2/top-headlines?country=us&q=${searchTerm}&apiKey=${apiKey}`;

  load.style.display = "block";
  newsDirectory.style.display = "none";

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      if (!data.articles) {
        newsDirectory.innerHTML = `<div class="card card-not-found bg-danger"> <div class="card-body not-found"> Data tidak ditemukan. </div></div>`;
      } else {
        const articles = data.articles;
        let newsHtml = "";
        console.log(data);
        if (articles.length === 0) {
          newsHtml = `<div class="card card-not-found bg-danger"> <div class="card-body not-found"> Data tidak ditemukan. </div></div>`;
        }
        else {
          articles.forEach((article) => {
            newsHtml += `
              <div class="card mb-3 shadow">
                <img src="${article.urlToImage}" class="card-img-top" alt="${article.title}">
                <div class="card-body">
                  <h5 class="card-title">${article.title}</h5>
                  <p class="card-text"><small class="text-muted">${article.source.name} - ${new Date(article.publishedAt).toLocaleDateString()}</small></p>
                  <p class="card-text">${article.description} </p>
                  <a href="${article.url}" target="_blank" class="btn btn-primary"> Read More... </a>
                </div>
              </div>`;
          });
        }

        newsDirectory.innerHTML = newsHtml;
      }

      load.style.display = "none";
      newsDirectory.style.display = "block";
    })
    .catch((error) => console.error(error));
}

let searchTerm = "";
searchBar.addEventListener("input", (event) => {
  const newSearchTerm = event.target.value.trim();
  if (searchTerm !== newSearchTerm) {
    searchTerm = newSearchTerm;
    updateNewsList(searchTerm);
  }
});

updateNewsList("");
